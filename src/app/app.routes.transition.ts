import { trigger, animate, style, group, animateChild, query, stagger, transition, state } from '@angular/animations';

export const routerTransition = trigger('routerTransition', [
    transition('* <=> *', [
        /* order */
        /* 1 */ query(':enter, :leave', style({position: 'fixed', width: '100%'}), {optional: true}),
        /* 2 */ query('.block', style({ opacity: 0 }), { optional: true }),
        /* 3 */ group([  // block executes in parallel
                    query(':enter', [
                        style({transform: 'translateX(-100%)', opacity: 0}),
                        animate('0.8s ease-in-out', style({transform: 'translateX(0%)', opacity: 1}))
                    ], {optional: true}),
                    query(':leave', [
                        style({transform: 'translateX(0%)', opacity: 0}),
                        animate('0.1s ease-in-out', style({transform: 'translateX(100%)', opacity: 0 }))
                    ] , {optional: true}),
                 ]),
         /* 4 */
            // query('.portfolio-items', stagger(900, [
            //     style({ transform: 'translateY(100px)' , opacity: 1 }),
            //     animate('2s ease-in-out',
            //         style({ transform: 'translateY(0px)', opacity: 0 })),
            // ]), {optional: true})

    ])
]);

export const sofbListItemAnimation =
  trigger('sofbListItemAnimation',
    [
      state('none, void',
        style({ width: '100%', height: '100%', opacity: 0 })
      ),
      state('maximum',
        style({ opacity: 0, width: '100%', height: '100%'})
      ),
      state('visible',
        style({ opacity: 1 })
      ),
      transition('none => maximum', animate('20ms')),
      transition('maximum => visible', animate('1000ms'))
    ]
  )

export const sofbListItemPlaceholderAnimation =
  trigger('sofbListItemPlaceholderAnimation',
    [
      state('none, void',
        style({ width: '100%', height: '100%', opacity: 0 })
      ),
      state('normal',
        style({ width: '100%', height: '100%', opacity: 1, })
      ),
      state('invisible',
        style({ display: '0'})
      ),
      transition('none => normal', animate('10ms')),
      transition('normal => invisible', animate('400ms'))
    ]
  )

export const sofbSubheaderAnimation =
  trigger('sofbSubheaderAnimation',
    [
      state('zero, void',
        style({ width: '100%', height: '0', opacity: 0 })
      ),
      state('mid',
        style({ height: '100%', opacity: 0, })
      ),
      state('done',
        style({ opacity: 1})
      ),
      transition('zero => mid', animate('10ms')),
      transition('mid => done', animate('2000ms'))
    ]
  )
