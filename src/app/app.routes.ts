import { Routes } from '@angular/router';

export const ROUTES: Routes = [
  { path: '',      loadChildren: './modules/home/home.module#HomeModule'},
  { path: 'about', loadChildren: './modules/about/about.module#AboutModule' },
  { path: 'contact', loadChildren: './modules/contact/contact.module#ContactModule'},
  { path: 'magazines', loadChildren: './modules/magazines/magazines.module#MagazinesModule'},
  { path: 'gallery', loadChildren: './modules/gallery/gallery.module#GalleryModule'},
  { path: 'advertising', loadChildren: './modules/advertising/advertising.module#SomethingModule'},
  { path: 'links', loadChildren: './modules/links/links.module#LinksModule'},
  { path: 'videos', loadChildren: './modules/videos/videos.module#VideosModule'},
  { path: '**',   loadChildren: './modules/no-content/no-content.module#NoContentModule'},
];
