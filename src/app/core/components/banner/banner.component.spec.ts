import {TestBed, async} from '@angular/core/testing';
import {RouterTestingModule} from '@angular/router/testing';
import {BannerComponent} from './banner.component';
import {CoreModule} from '../../core.module';

describe('CoreModule - BannerComponent', () => {
    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [CoreModule, RouterTestingModule],
            declarations: [],
        }).compileComponents();
    });

    it('should create the component', () => {
        const fixture = TestBed.createComponent(BannerComponent);
        const app = fixture.debugElement.componentInstance;
        expect(app).toBeTruthy();
    });
});
