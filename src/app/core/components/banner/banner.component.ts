import {
    Component,
    OnInit
} from '@angular/core';
import {environment} from '../../../../environments/environment';

@Component({
    selector: 'sofb-banner',
    styleUrls: ['./banner.component.css'],
    templateUrl: './banner.component.html'
})
export class BannerComponent implements OnInit {
    sofbAdSlides = environment.content.global.banner.sofbAdSlides;
    poloAndBikeSlides = environment.content.global.banner.poloAndBikeSlides;
    kappsteinSlides = environment.content.global.banner.kappsteinSlides;
    bombtrackSlides = environment.content.global.banner.bombtrackSlides;

    constructor() {
    }
    public ngOnInit() {
    }
}
