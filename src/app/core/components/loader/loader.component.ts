import {Component, Input, OnInit} from '@angular/core';

@Component({
    selector: 'sofb-loader',
    templateUrl: 'loader.component.html',
    styleUrls: ['./loader.component.scss'],

})
export class LoaderComponent implements OnInit {
    @Input() public loading = false;
    constructor() { }

    public ngOnInit() { }

}
