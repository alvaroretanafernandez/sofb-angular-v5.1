import {
    Component,
    OnInit, ViewEncapsulation
} from '@angular/core';
import {environment} from '../../../../environments/environment';

@Component({
    selector: 'sofb-pre-footer',
    styleUrls: ['./prefooter.component.scss'],
    templateUrl: './prefooter.component.html',
    encapsulation: ViewEncapsulation.None
})
export class PreFooterComponent implements OnInit {
    staticContent: any = environment.content.global.prefooter.content;
    social: any = environment.content.global.social;
    constructor() {
    }

    public ngOnInit() {
    }
}
