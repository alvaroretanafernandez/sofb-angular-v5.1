import {Component, ViewChild} from '@angular/core';
import {NavigationCancel, NavigationEnd, NavigationError, NavigationStart, Router} from '@angular/router';
import {NavbarComponent} from './navbar/navbar.component';
import {routerTransition} from '../../app.routes.transition';
import {Store} from '@ngrx/store';
import * as fromRoot from '../../shared-store/store';

@Component({
    selector: 'app-root',
    animations: [routerTransition],
    templateUrl: './app.component.html'
})
export class AppComponent {
    title = 'Stoked On Fixed Bikes';
    public isCollapsed = true;
    public loading: Boolean = true;
    @ViewChild('nav') public nav: NavbarComponent;

    constructor(public router: Router, private store: Store<fromRoot.State>) {
        router.events.subscribe((event: any) => {
            this.navigationInterceptor(event);
        });
    }

    getState(outlet) {
        return outlet.activatedRouteData.state;
    }

    navigationInterceptor(event: any): void {
        if (event instanceof NavigationStart) {
            this.loading = true;
        }
        if (event instanceof NavigationEnd) {
            this.loading = false;
            this.nav.isCollapsed = true;
            document.body.scrollTop = 0;
            this.title = this.router.url.split('/')[1];
        }
        // Set loading state to false in both of the below events to hide the spinner in case a request fails
        if (event instanceof NavigationCancel || event instanceof NavigationError) {
            this.loading = false;
        }
    }

}
