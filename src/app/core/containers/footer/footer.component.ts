import {
    Component,
    OnInit
} from '@angular/core';
import {environment} from '../../../../environments/environment';

@Component({
    // The selector is what angular internally uses
    // for `document.querySelectorAll(selector)` in our index.html
    // where, in this case, selector is the string 'home'
    selector: 'sofb-footer',  // <sofb-footer></sofb-footer>
    // We need to tell Angular's Dependency Injection which providers are in our app.

    // Our list of styles in our component. We may add more to compose many styles together
    styleUrls: ['./footer.component.css'],
    // Every Angular template is first compiled by the browser before Angular runs it's compiler
    templateUrl: './footer.component.html'
})
export class FooterComponent implements OnInit {
    // Set our default values
    public localState = {value: ''};

    public footerMenu = environment.content.global.menu;
    // TypeScript public modifiers
    constructor() {
    }

    public ngOnInit() {
        // console.log('hello `Footer` component');
        // this.title.getData().subscribe(data => this.data = data);
    }


}
