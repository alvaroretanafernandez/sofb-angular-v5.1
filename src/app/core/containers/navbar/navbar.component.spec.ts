import {TestBed, async} from '@angular/core/testing';
import {RouterTestingModule} from '@angular/router/testing';
import {NavbarComponent} from './navbar.component';
import {CoreModule} from '../../core.module';

describe('Module - Core - NavbarComponent', () => {
    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [CoreModule, RouterTestingModule],
            declarations: [],
        }).compileComponents();
    });

    it('should create the component', () => {
        const fixture = TestBed.createComponent(NavbarComponent);
        const app = fixture.debugElement.componentInstance;
        expect(app).toBeTruthy();
    });
});
