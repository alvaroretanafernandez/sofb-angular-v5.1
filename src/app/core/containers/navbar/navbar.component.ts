import {
    Component, Input,
    OnInit
} from '@angular/core';
import {environment} from '../../../../environments/environment';

@Component({
    selector: 'sofb-navbar',
    styleUrls: [ './navbar.component.css' ],
    templateUrl: './navbar.component.html'
})
export class NavbarComponent implements OnInit {
    @Input() public isCollapsed;
    menu: any = environment.content.global.menu;
    constructor() {}
    public ngOnInit() {
    }
}
