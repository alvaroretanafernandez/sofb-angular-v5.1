/* tslint:disable:member-ordering no-unused-variable */
import {
  NgModule,
  Optional, SkipSelf
} from '@angular/core';
import {RouterModule} from '@angular/router';
import {CommonModule} from '@angular/common';
import {NavbarComponent} from './containers/navbar/navbar.component';
import {FooterComponent} from './containers/footer/footer.component';
import {PreFooterComponent} from './components/prefooter/prefooter.component';
import {BannerComponent} from './components/banner/banner.component';
import {LoaderComponent} from './components/loader/loader.component';
import {AppComponent} from './containers/app.component';
import {SharedLibModule} from '../shared-lib/shared-lib.module';
import {SharedModule} from '../shared/shared.module';

@NgModule({
  imports: [
      CommonModule,
      RouterModule,
      SharedLibModule,
      SharedModule,
  ],
  declarations: [
      AppComponent,
      NavbarComponent,
      LoaderComponent,
      FooterComponent,
      PreFooterComponent,
      BannerComponent
  ],
  exports: [
      AppComponent,
      NavbarComponent,
      FooterComponent,
      PreFooterComponent,
      BannerComponent,
      LoaderComponent,
      // modules
      RouterModule,
      SharedLibModule,
      SharedModule
  ],
})
export class CoreModule {
  constructor(@Optional() @SkipSelf() parentModule: CoreModule) {
    if (parentModule) {
      throw new Error(
        'CoreModule is already loaded. Import it in the AppModule only');
    }
  }
    static forRoot() {
        return {
            ngModule: CoreModule,
            providers: [],
        };
    }
}

