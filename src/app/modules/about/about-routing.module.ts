import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {AboutComponent} from './containers/about';

const routes: Routes = [
    { path: '', component: AboutComponent,  pathMatch: 'full' , data: { state: 'about' }},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AboutRoutingModule { }
