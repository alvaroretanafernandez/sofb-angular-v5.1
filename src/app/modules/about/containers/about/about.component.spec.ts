
// Load the implementations that should be tested
import { AboutComponent } from './about.component';

import { TestBed, async } from '@angular/core/testing';

describe('App - AboutComponent', () => {
    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [
                AboutComponent
            ],
        }).compileComponents();
    });

    it('should create the component', () => {
        const fixture = TestBed.createComponent(AboutComponent);
        const app = fixture.debugElement.componentInstance;
        expect(app).toBeTruthy();
    });
});
