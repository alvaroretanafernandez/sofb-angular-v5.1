import {
    Component,
    OnInit
} from '@angular/core';
import {environment} from '../../../../../environments/environment';

@Component({
    selector: 'about-sofb',
    templateUrl: './about.component.html',
    styleUrls: ['./about.component.css']
})
export class AboutComponent implements OnInit {
    staticContent = environment.content.about;
    constructor() {
    }
    public ngOnInit() {
    }
}
