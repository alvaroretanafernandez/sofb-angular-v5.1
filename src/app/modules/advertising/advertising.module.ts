import {CommonModule} from '@angular/common';
import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
import {routes} from './advertising.routes';
import {AdvertisingComponent} from './containers/advertising.component';
import {SharedLibModule} from '../../shared-lib/shared-lib.module';
import {SharedModule} from '../../shared/shared.module';
import { OldAdvertComponent } from './components/old-advert/old-advert.component';
import { AdSectionComponent } from './components/ad-section/ad-section.component';

@NgModule({
    declarations: [
        // Components / Directives/ Pipes
        AdvertisingComponent,
      OldAdvertComponent,
      AdSectionComponent
    ],
    imports: [
        CommonModule,
        SharedModule,
        SharedLibModule,
        RouterModule.forChild(routes),
    ]
})
export class SomethingModule {
    public static routes = routes;
}
