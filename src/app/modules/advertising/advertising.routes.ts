import { AdvertisingComponent } from './containers/advertising.component';

export const routes = [
  { path: '', children: [
    { path: '', component: AdvertisingComponent, data: { state: 'advertising' } }
  ]},
];
