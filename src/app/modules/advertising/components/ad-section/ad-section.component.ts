import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'sofb-ad-section',
  templateUrl: './ad-section.component.html',
  styleUrls: ['./ad-section.component.css']
})
export class AdSectionComponent implements OnInit {
  @Input() ad;
  constructor() { }

  ngOnInit() {
  }

}
