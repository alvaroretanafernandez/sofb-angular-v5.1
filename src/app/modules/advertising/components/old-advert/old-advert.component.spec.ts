import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OldAdvertComponent } from './old-advert.component';

describe('OldAdvertComponent', () => {
  let component: OldAdvertComponent;
  let fixture: ComponentFixture<OldAdvertComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OldAdvertComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OldAdvertComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
