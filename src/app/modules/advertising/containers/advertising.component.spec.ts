
import { TestBed, async } from '@angular/core/testing';
import {AdvertisingComponent} from './advertising.component';

describe('Module - Advertising - AdvertisingComponent', () => {
    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [
                AdvertisingComponent
            ],
        }).compileComponents();
    });

    it('should create the component', () => {
        const fixture = TestBed.createComponent(AdvertisingComponent);
        const app = fixture.debugElement.componentInstance;
        expect(app).toBeTruthy();
    });
});
