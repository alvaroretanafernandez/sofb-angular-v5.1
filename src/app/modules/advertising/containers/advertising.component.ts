import {
    Component,
    OnInit,
} from '@angular/core';
import {environment} from '../../../../environments/environment';
import { animate, query, stagger, style, transition, trigger } from '@angular/animations';
import { photosAnimationHorizontal, photosAnimationVertical } from '../../../shared/animations/app.list-items.animations';

@Component({
    selector: 'sofb-advertising',
    styleUrls: ['./advertising.component.css'],
    templateUrl: './advertising.component.html',
    animations: [
      trigger('photosAnimationHorizontal', [
        transition('* => *', [
          query('sofb-ad-section', style({ transform: 'translateX(-300%)', opacity: 0.6, width: '0%'})),
          query('sofb-ad-section',
            stagger('1ms', [
              animate('2200ms', style({ transform: 'translateX(0)', opacity: 1, width: '100%'}))
            ]))
        ])
      ]),
      trigger('photosAnimationVertical', [
        transition('* => *', [
          query('sofb-old-advert', style({ transform: 'translateY(300%)', opacity: 0.6, width: '0%'})),
          query('sofb-old-advert',
            stagger('22ms', [
              animate('1200ms', style({ transform: 'translateY(0)', opacity: 1, width: '100%'}))
            ]))
        ])
      ])
    ]
})
export class AdvertisingComponent implements OnInit {
    header: any = environment.content.advertising.header;
    body: any = environment.content.advertising.body;
    constructor() {
    }
    ngOnInit() {
    }
}
