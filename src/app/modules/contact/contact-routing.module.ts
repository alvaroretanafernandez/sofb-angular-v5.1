import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {ContactComponent} from './containers/contact/contact.component';

const routes: Routes = [
    { path: '', component: ContactComponent,  pathMatch: 'full' , data: { state: 'contact' }},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ContactRoutingModule { }
