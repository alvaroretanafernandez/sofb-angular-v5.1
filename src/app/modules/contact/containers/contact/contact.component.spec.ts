import {TestBed, async} from '@angular/core/testing';
import {ContactComponent} from './contact.component';

describe('App - ContactComponent', () => {
    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [
                ContactComponent
            ],
        }).compileComponents();
    });

    it('should create the component', () => {
        const fixture = TestBed.createComponent(ContactComponent);
        const app = fixture.debugElement.componentInstance;
        expect(app).toBeTruthy();
    });
    it(`should have as url banner 'https://s3.eu-central-1.amazonaws.com/sofbheaders/8.jpg!'`, () => {
        const fixture = TestBed.createComponent(ContactComponent);
        const app = fixture.debugElement.componentInstance;
        expect(app.banner).toEqual('https://s3.eu-central-1.amazonaws.com/sofbheaders/8.jpg');
    });
});
