import {TestBed, async} from '@angular/core/testing';
import {GalleryComponent} from './gallery.component';
import {GalleryModule} from '../gallery.module';

describe('Module - Gallery - GalleryComponent', () => {
    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [GalleryModule],
            declarations: [],
        }).compileComponents();
    });

    it('should create the component', () => {
        const fixture = TestBed.createComponent(GalleryComponent);
        const app = fixture.debugElement.componentInstance;
        expect(app).toBeTruthy();
    });
});
