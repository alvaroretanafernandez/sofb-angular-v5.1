import {
    Component,
    OnInit,
} from '@angular/core';
import {environment} from '../../../../environments/environment';
import { animate, query, stagger, style, transition, trigger } from '@angular/animations';

@Component({
    selector: 'sofb-gallery',
    styleUrls: ['./gallery.component.css'],
    templateUrl: './gallery.component.html'
})
export class GalleryComponent implements OnInit {

    gallerySlides = environment.content.gallery.body.slides;

    constructor(
    ) {}

    public ngOnInit() {
        // this.resolveFeed();
    }

}
