import {CommonModule} from '@angular/common';
import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
import {routes} from './gallery.routes';
import {GalleryComponent} from './containers/gallery.component';
import {SharedModule} from '../../shared/shared.module';
import {SharedLibModule} from '../../shared-lib/shared-lib.module';

@NgModule({
    imports: [
        CommonModule,
        SharedLibModule,
        RouterModule.forChild(routes),
    ],
    declarations: [
        // Components / Directives/ Pipes
        GalleryComponent,
    ],
    providers: []
})
export class GalleryModule {
    public static routes = routes;
}
