import { GalleryComponent } from './containers/gallery.component';

export const routes = [
  { path: '', children: [
    { path: '', component: GalleryComponent, data: { state: 'gallery' } }
  ]},
];
