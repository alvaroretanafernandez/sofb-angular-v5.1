import { TestBed, async } from '@angular/core/testing';
import { HomeComponent } from './home.component';
import {SharedModule} from '../../../../shared/shared.module';
import {HomeModule} from '../../home.module';

describe('App - HomeComponent', () => {
    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [
                HomeModule,
                SharedModule
            ],
        }).compileComponents();
    });

    it('should create the component', () => {
        const fixture = TestBed.createComponent(HomeComponent);
        const app = fixture.debugElement.componentInstance;
        expect(app).toBeTruthy();
    });
});
