import { Component, OnInit } from '@angular/core';
import {environment} from '../../../../../environments/environment';

@Component({
    selector: 'sofb-home',
    styleUrls: ['./home.component.css'],
    templateUrl: './home.component.html'
})
export class HomeComponent implements OnInit {

    staticContent = environment.content.home;

    constructor() {
    }

    public ngOnInit() {
    }

}
