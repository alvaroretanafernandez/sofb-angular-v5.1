import { AfterViewInit, ChangeDetectorRef, Component, Input, OnInit } from '@angular/core';
import { animate, keyframes, query, stagger, state, style, transition, trigger } from '@angular/animations';

@Component({
  selector: 'sofb-link-item',
  templateUrl: './link-item.component.html',
  styleUrls: ['./link-item.component.css'],
  animations: [
  
  ]
})
export class LinkItemComponent implements OnInit {
  @Input() item;
  state = 'inactive';
  constructor(private cdr: ChangeDetectorRef) { }

  ngOnInit() {
  }
}
