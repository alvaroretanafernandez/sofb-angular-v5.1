import { TestBed, async } from '@angular/core/testing';
import {LinksComponent} from './links.component';
import {AppModule} from '../../../app.module';
import {LinksModule} from '../links.module';
import {RouterTestingModule} from '@angular/router/testing';
import {HttpModule, XHRBackend} from '@angular/http';
import {MockBackend} from '@angular/http/testing';

describe('Module - Links - LinksComponent', () => {
    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [AppModule, LinksModule, RouterTestingModule, HttpModule],
            providers: [
                { provide: XHRBackend, useClass: MockBackend }
            ]
        }).compileComponents();
    });

    it('should create the component', () => {
        const fixture = TestBed.createComponent(LinksComponent);
        const app = fixture.debugElement.componentInstance;
        expect(app).toBeTruthy();
    });
});
