import {
  Component,
  OnInit,
} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {select, Store} from '@ngrx/store';
import * as reducers from '../../../shared-store/reducers';
import * as linkActions from '../../../shared-store/actions/links.actions';
import {Link} from '../../../shared/models/link.model';
import { animate, query, stagger, style, transition } from '@angular/animations';
import { trigger } from '@angular/animations';
import { photosAnimationVertical } from '../../../shared/animations/app.list-items.animations';


@Component({
  selector: 'sofb-links',
  templateUrl: './links.component.html',
  styleUrls: ['./links.component.css'],
  animations: [
    trigger('photosAnimation', [
      transition('* => *', [
        query('sofb-link-item', style({ transform: 'translateX(300%)', opacity: 0.6, width: '0%', height: '0%'})),
        query('sofb-link-item',
          stagger('1ms', [
            animate('1200ms', style({ transform: 'translateX(0)', opacity: 1, width: '100%', height: '100%'}))
          ]))
      ])
    ])
  ]
})
export class LinksComponent implements OnInit {

  links$: Observable<Link[]>;
  constructor(
      private store: Store<reducers.State>
  ) {
      this.links$ = this.store.pipe(select(reducers.getLinkItems));

  }
  public ngOnInit() {
      this.store.dispatch(new linkActions.LinksLoad());
  }

}
