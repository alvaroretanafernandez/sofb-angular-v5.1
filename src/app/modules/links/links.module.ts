import {CommonModule} from '@angular/common';
import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
import {routes} from './links.routes';
import {LinksComponent} from './containers/links.component';
import {SharedModule} from '../../shared/shared.module';
import {LinksEffects} from '../../shared-store/effects/links.effects';
import {StoreModule} from '@ngrx/store';
import {EffectsModule} from '@ngrx/effects';
import {LinksReducers} from '../../shared-store/reducers';
import { LinkItemComponent } from './components/link-item/link-item.component';

@NgModule({
    declarations: [
        LinksComponent,
        LinkItemComponent
    ],
    imports: [
        CommonModule,
        SharedModule,
        RouterModule.forChild(routes),
        /**
         * StoreModule.forFeature is used for composing state
         * from feature modules. These modules can be loaded
         * eagerly or lazily and will be dynamically added to
         * the existing state.
         */
        StoreModule.forFeature('links', LinksReducers),
        /**
         * Effects.forFeature is used to register effects
         * from feature modules. Effects can be loaded
         * eagerly or lazily and will be started immediately.
         *
         * All Effects will only be instantiated once regardless of
         * whether they are registered once or multiple times.
         */
        EffectsModule.forFeature([LinksEffects])
    ],
})
export class LinksModule {
    public static routes = routes;
}
