import { LinksComponent } from './containers/links.component';

export const routes = [
  { path: '', component: LinksComponent,  pathMatch: 'full' , data: { state: 'links' }}
];
