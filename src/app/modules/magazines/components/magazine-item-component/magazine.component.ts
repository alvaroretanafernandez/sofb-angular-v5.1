import {
  Component,
  OnInit,
  Input, ChangeDetectorRef, AfterViewInit
} from '@angular/core';
import { Magazine } from '../../../../shared/services/magazines-service/magazines.service';
import { COMMON_PIPES } from '@angular/common/src/pipes';
import { Router } from '@angular/router';
import { Store } from '@ngrx/store';
import * as magazineReducers from '../../../../shared-store/reducers';
import * as magazineActions from '../../../../shared-store/actions/magazines.actions';
import { sofbListItemAnimation } from '../../../../app.routes.transition';

@Component({
  selector: 'sofb-magazine-item',  // <sofb-magazine></sofb-magazine>
  styleUrls: ['./magazine.component.scss'],
  templateUrl: './magazine.component.html',
  animations: [sofbListItemAnimation]
})
export class MagazineComponent implements OnInit, AfterViewInit {

  @Input() magazineData: Magazine;
  @Input() hov: Boolean = true;
  animationState = 'none';
  constructor(private cdr: ChangeDetectorRef, private router: Router, private store: Store<magazineReducers.State>) {
  }

  public ngOnInit() {
  }

  onClickMagazine(id) {
    this.store.dispatch(new magazineActions.MagazineSelected(id));
    this.router.navigate(['./magazines/' + id]);
  }
  
  ngAfterViewInit() {
    this.animationState = 'maximum';
    setTimeout(() => {this.animationState = 'visible'; }, 100);
    this.cdr.detectChanges();
  }
  
}
