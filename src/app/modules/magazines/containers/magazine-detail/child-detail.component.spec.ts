import { TestBed, async } from '@angular/core/testing';
import {MagazinesModule} from '../../magazines.module';
import {ChildDetailComponent} from './child-detail.component';
import {CoreModule} from '../../../../core/core.module';
import {SharedModule} from '../../../../shared/shared.module';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {RouterTestingModule} from '@angular/router/testing';
import {MagazineReducers} from '../../../../shared-store/reducers';
import {StoreModule} from '@ngrx/store';
import {EffectsModule} from '@ngrx/effects';
import {MagazinesEffects} from '../../../../shared-store/effects/magazines.effects';

describe('MagazinesModule - ChildDetailComponent', () => {
    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [CoreModule, SharedModule, MagazinesModule, HttpClientTestingModule, RouterTestingModule,
                EffectsModule.forRoot([]),
                StoreModule.forRoot({}), StoreModule.forFeature('magazines', MagazineReducers), EffectsModule.forFeature([MagazinesEffects])],
            declarations: [
            ],
            providers: [

            ]
        }).compileComponents();
    });

    it('should create the component', () => {
        const fixture = TestBed.createComponent(ChildDetailComponent);
        const app = fixture.debugElement.componentInstance;
        expect(app).toBeTruthy();
    });
});
