import {
    Component,
    OnInit,
    Output,
    ViewChild,
    EventEmitter,
    Input
} from '@angular/core';
import {ActivatedRoute, ActivatedRouteSnapshot} from '@angular/router';
import {Magazine, MagazinesService} from '../../../../shared/services/magazines-service/magazines.service';
import {SafePipe} from '../../../../shared/pipes/safe.pipe';
import {DatePipe} from '@angular/common';
import {select, Store} from '@ngrx/store';
import * as magazineReducers from '../../../../shared-store/reducers';

@Component({
    selector: 'sofb-child-detail',
    templateUrl: './child-detail.component.html',
    styleUrls: ['./child-detail.component.scss'],
    providers: [SafePipe, DatePipe]
})
export class ChildDetailComponent implements OnInit {

    public currentMagazine: number = 1;
    public currentMagazinePlus: number = 0;
    public currentMagazineMinus: number = 0;
    public magazine: Magazine;
    public frame: string = '';
    public showIframe: boolean = false;

    @Output() frameChanged = new EventEmitter();

    constructor(
        private store: Store<magazineReducers.State>,
        public activatedRoute: ActivatedRoute) {
        this.activatedRouterInterceptor();
    }
    get source() {
        return this.magazine ? this.magazine.frame : '';
    }
    public ngOnInit() {
        // this.frame = this.magazine.frame;
    }
    activatedRouterInterceptor() {
        this.activatedRoute.params.subscribe(params => {
            this.magazine = null;
          this.showIframe = false;
            this.currentMagazine = parseInt(params['id'], 10);
            this.currentMagazinePlus = this.currentMagazine + 1;
            this.currentMagazineMinus = this.currentMagazine - 1;
            this.magazine = this.activatedRoute.snapshot.data['magazine'] || [];
            console.log(this.activatedRoute.snapshot.data);
            setTimeout(() => {
                this.frame = this.magazine.frame;
                this.showIframe = true;
            }, 100);
        });
    }
}
