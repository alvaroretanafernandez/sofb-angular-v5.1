import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot } from '@angular/router';
import {Observable} from 'rxjs/Observable';
import {MagazinesService} from '../../../../shared/services/magazines-service/magazines.service';

@Injectable()
export class ChildDetailResolver implements Resolve<any> {
    constructor( private magazinesService: MagazinesService) {
    }
    resolve(route: ActivatedRouteSnapshot): Observable<any> {
        return this.magazinesService.getMagazine(route.params['id']);
    }
}
