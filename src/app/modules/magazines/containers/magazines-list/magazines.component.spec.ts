import { TestBed, async } from '@angular/core/testing';
import {MagazinesComponent} from './magazines.component';
import {RouterTestingModule} from '@angular/router/testing';
import {HttpModule, XHRBackend} from '@angular/http';
import {MockBackend} from '@angular/http/testing';
import {MagazinesModule} from '../../magazines.module';
import {MagazineReducers} from '../../../../shared-store/reducers';
import {StoreModule} from '@ngrx/store';
import {MagazinesEffects} from '../../../../shared-store/effects/magazines.effects';
import {Actions, EffectsModule} from '@ngrx/effects';
import {HttpClientTestingModule} from '@angular/common/http/testing';

describe('Module - Magazines - MagazinesComponent', () => {
    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [ MagazinesModule, RouterTestingModule, HttpClientTestingModule,
                StoreModule.forRoot({}),
                StoreModule.forFeature('magazines', MagazineReducers),
                EffectsModule.forRoot([]),
                EffectsModule.forFeature([MagazinesEffects])],
            declarations: [
                //
            ],
            providers: [
            ]
        }).compileComponents();
    });

    it('should create the component', () => {
        const fixture = TestBed.createComponent(MagazinesComponent);
        const app = fixture.debugElement.componentInstance;
        expect(app).toBeTruthy();
    });
});
