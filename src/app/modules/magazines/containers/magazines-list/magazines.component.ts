import {
    Component,
    OnInit,
    QueryList,
    ViewChildren
} from '@angular/core';
import {MagazineComponent} from '../../components/magazine-item-component/magazine.component';

import * as magazineReducers from '../../../../shared-store/reducers/index'
import * as magazineActions from '../../../../shared-store/actions/magazines.actions'
import {select, Store} from '@ngrx/store';
import {Magazine} from '../../../../shared/models/magazine.model';
import {Observable} from 'rxjs/Observable';


@Component({
    selector: 'magazines',
    templateUrl: './magazines.component.html',
    styleUrls: ['./magazines.component.css']

})
export class MagazinesComponent implements OnInit {
    loaded = false;
    magazines$: Observable<Magazine[]>;
    @ViewChildren(MagazineComponent) magazine: QueryList<MagazineComponent>;
    constructor(private store: Store<magazineReducers.State>) {
        this.magazines$ = this.store.pipe(select(magazineReducers.getMagazineItems));
    }
    public ngOnInit() {
        this.store.dispatch(new magazineActions.MagazinesLoad());
        this.magazines$.subscribe((data) => {
          setTimeout(() => { this.loaded = true; }, 1500);
        });
    }
}
