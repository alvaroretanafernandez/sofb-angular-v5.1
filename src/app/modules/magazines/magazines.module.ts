import {CommonModule} from '@angular/common';
import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
import {routes} from './magazines.routes';
import {MagazinesComponent} from './containers/magazines-list/magazines.component';
import {ChildDetailResolver} from './containers/magazine-detail/child-detail.resolver';
import {ChildDetailComponent} from './containers/magazine-detail/child-detail.component';
import {SharedModule} from '../../shared/shared.module';
import {MagazineComponent} from './components/magazine-item-component/magazine.component';
import {StoreModule} from '@ngrx/store';
import {EffectsModule} from '@ngrx/effects';
import {MagazineReducers} from '../../shared-store/reducers';
import {MagazinesEffects} from '../../shared-store/effects/magazines.effects';

@NgModule({

    imports: [
        CommonModule,
        SharedModule,
        RouterModule.forChild(routes),
        /**
         * StoreModule.forFeature is used for composing state
         * from feature modules. These modules can be loaded
         * eagerly or lazily and will be dynamically added to
         * the existing state.
         */
        StoreModule.forFeature('magazines', MagazineReducers),
        /**
         * Effects.forFeature is used to register effects
         * from feature modules. Effects can be loaded
         * eagerly or lazily and will be started immediately.
         *
         * All Effects will only be instantiated once regardless of
         * whether they are registered once or multiple times.
         */
        EffectsModule.forFeature([MagazinesEffects])
    ],
    declarations: [
        MagazinesComponent,
        MagazineComponent,
        ChildDetailComponent
    ],
    providers: [ChildDetailResolver]
})
export class MagazinesModule {
    public static routes = routes;
}
