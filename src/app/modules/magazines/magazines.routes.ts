import { MagazinesComponent } from './containers/magazines-list/magazines.component';
import { ChildDetailComponent } from './containers/magazine-detail/child-detail.component';
import { ChildDetailResolver } from './containers/magazine-detail/child-detail.resolver';
export const routes = [
  { path: '', children: [
    { path: '', component: MagazinesComponent, data: {state: ':enter'}},
    { path: ':id', component: ChildDetailComponent, pathMatch: 'full' , resolve: { magazine: ChildDetailResolver }, data: { state: 'magazine' } },
  ]},
];
