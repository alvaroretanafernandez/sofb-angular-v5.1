import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {NoContentComponent} from './containers/no-content/no-content.component';

const routes: Routes = [
    { path: '', component: NoContentComponent , data: { state: '404' }},

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class NoContentRoutingModule { }
