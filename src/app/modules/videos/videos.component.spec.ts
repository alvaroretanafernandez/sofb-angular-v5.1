import { TestBed, async } from '@angular/core/testing';
import {AppModule} from '../../app.module';
import {RouterTestingModule} from '@angular/router/testing';
import {HttpModule, XHRBackend} from '@angular/http';
import {MockBackend} from '@angular/http/testing';
import {VideosModule} from './videos.module';
import {VideosComponent} from './videos.component';

describe('Module - Videos - VideosComponent', () => {
    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [AppModule, VideosModule, RouterTestingModule, HttpModule],
            declarations: [
                //
            ],
            providers: [
                { provide: XHRBackend, useClass: MockBackend }
            ]
        }).compileComponents();
    });

    it('should create the component', () => {
        const fixture = TestBed.createComponent(VideosComponent);
        const app = fixture.debugElement.componentInstance;
        expect(app).toBeTruthy();
    });
});
