import {
    Component,
    OnInit,
} from '@angular/core';
import {Video, VideosService} from '../../shared/services/videos-service/videos.service';


@Component({
    selector: 'sofb-videos',
    templateUrl: './videos.component.html',
    styleUrls: ['./videos.component.css']
})
export class VideosComponent implements OnInit {

    videos: Video[];
    YT_URL = 'https://www.youtube.com/embed/';
    VI_URL = 'https://player.vimeo.com/video/';
    frame: any;
    public showIframe: Boolean = false;

    constructor(
        public videosService: VideosService
    ) {
    }

    public ngOnInit() {
        this.resolveLinks();
    }

    resolveLinks() {
        this.videosService.getVideos().subscribe(data => this.videos = data);
    }

    loadVideo(video) {
        this.showIframe = false;
        if (video.type === 'youtube') {
            this.frame = this.YT_URL + video.videoId;
        } else {
            this.frame = this.VI_URL + video.videoId + '?autoplay=0';
        }
        setTimeout(() => {
            this.showIframe = true;
            document.body.scrollTop = 0;
        }, 100);

    }

}
