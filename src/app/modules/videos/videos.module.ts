import {CommonModule} from '@angular/common';
import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
import {routes} from './videos.routes';
import {VideosComponent} from './videos.component';
import {SharedModule} from '../../shared/shared.module';


@NgModule({
    declarations: [
        VideosComponent,
    ],
    imports: [
        CommonModule,
        SharedModule,
        RouterModule.forChild(routes),
    ],
})
export class VideosModule {
    public static routes = routes;
}
