import { VideosComponent } from './videos.component';

export const routes = [
  { path: '', component: VideosComponent,  pathMatch: 'full' , data: { state: 'videos' }},
];
