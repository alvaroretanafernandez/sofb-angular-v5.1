import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {CarouselModule} from 'ngx-bootstrap/carousel/carousel.module';
import {CollapseModule} from 'ngx-bootstrap/collapse/collapse.module';
import {FormsModule} from '@angular/forms';
import {AwesomePipe} from '../shared/pipes/awesome.pipe';
import {SafePipe} from '../shared/pipes/safe.pipe';
import {HighlightDirective} from '../shared/pipes/highlight.directive';

@NgModule({
  imports: [
    CommonModule,
      CollapseModule.forRoot(),
      CarouselModule.forRoot(),
      FormsModule,
  ],
    exports: [
        FormsModule,
        CollapseModule,
        CarouselModule,
    ],
})
export class SharedLibModule { }
