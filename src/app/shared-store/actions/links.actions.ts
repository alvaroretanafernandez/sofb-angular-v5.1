import { Action } from '@ngrx/store';
import { Link } from '../../shared/models/link.model';

export enum LinksActionTypes {
    LinksLoad = '[Links] LinksLoad',
    LinksLoadSuccess = '[Links] LinksLoadSuccess',
    LinksLoadError = '[Links] LinksLoadError',
}
export type LinksActionTypesUnion = LinksLoad | LinksLoadSuccess | LinksLoadError;


export class LinksLoad implements Action {
    readonly type = LinksActionTypes.LinksLoad;
}

export class LinksLoadSuccess implements Action {
    readonly type = LinksActionTypes.LinksLoadSuccess;
    constructor(public payload: Link) {
    }
}

export class LinksLoadError implements Action {
    readonly type = LinksActionTypes.LinksLoadError;
    constructor(public payload: any) {
    }
}
