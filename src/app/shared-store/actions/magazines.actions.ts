import { Action } from '@ngrx/store';
import { Magazine } from '../../shared/models/magazine.model';

export enum MagazinesActionTypes {
    MagazinesLoad = '[Magazines] MagazinesLoad',
    MagazinesLoadSuccess = '[Magazines] MagazinesLoadSuccess',
    MagazinesLoadError = '[Magazines] MagazinesLoadError',
    MagazineSelected = '[Magazine] MagazineSelected'
}
export type MagazinesActionTypesUnion = MagazinesLoad | MagazinesLoadSuccess | MagazinesLoadError | MagazineSelected;

export class MagazineSelected implements Action {
    readonly type = MagazinesActionTypes.MagazineSelected;
    constructor(public payload: any) {
    }
}

export class MagazinesLoad implements Action {
    readonly type = MagazinesActionTypes.MagazinesLoad;
}

export class MagazinesLoadSuccess implements Action {
    readonly type = MagazinesActionTypes.MagazinesLoadSuccess;
    constructor(public payload: Magazine[]) {
    }
}

export class MagazinesLoadError implements Action {
    readonly type = MagazinesActionTypes.MagazinesLoadError;
    constructor(public payload: any) {
    }
}
