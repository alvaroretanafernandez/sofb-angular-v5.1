import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { Action } from '@ngrx/store';
import { Observable, of } from 'rxjs';
import { catchError, map, switchMap } from 'rxjs/operators';
import {LinksService} from '../../shared/services/links-service/links.service';
import {LinksActionTypes, LinksLoadError, LinksLoadSuccess} from '../actions/links.actions';

@Injectable()
export class LinksEffects {
    constructor( private actions$: Actions,
                 private linksService: LinksService) {}
    @Effect()
    loadLinks$: Observable<Action> = this.actions$.pipe(
        ofType(LinksActionTypes.LinksLoad),
        switchMap(() =>
            this.linksService.getLinks()
            .pipe(
                map(links => new LinksLoadSuccess(links)),
                catchError(error => of(new LinksLoadError(error)))
            )
        )
    );
}


