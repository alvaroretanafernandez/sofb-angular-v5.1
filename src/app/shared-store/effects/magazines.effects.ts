import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { Action } from '@ngrx/store';
import { Observable, of } from 'rxjs';
import { catchError, map, switchMap } from 'rxjs/operators';
import { MagazinesService } from '../../shared/services/magazines-service/magazines.service';
import { MagazinesActionTypes, MagazinesLoadError, MagazinesLoadSuccess } from '../actions/magazines.actions';

@Injectable()
export class MagazinesEffects {
  @Effect()
  loadMagazines$: Observable<Action> = this.actions$.pipe(
    ofType(MagazinesActionTypes.MagazinesLoad),
    switchMap(() =>
      this.magazinesService.getMagazines()
        .pipe(
          map(magazines => new MagazinesLoadSuccess(magazines)),
          catchError(error => of(new MagazinesLoadError(error)))
        )
    )
  );

  constructor(private actions$: Actions,
              private magazinesService: MagazinesService) {
  }
}


