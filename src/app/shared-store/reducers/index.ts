import { createSelector, createFeatureSelector, ActionReducerMap } from '@ngrx/store';
import * as fromMagazines from './magazines.reducer';
import * as fromLinks from './links.reducer';
import * as fromRoot from '../store/index';


export interface MagazinesState {
    magazines: fromMagazines.State;
}

export interface LinksState {
    links: fromLinks.State;
}

export interface State extends fromRoot.State {
    magazines: MagazinesState,
    links: LinksState
}

export const MagazineReducers: ActionReducerMap<MagazinesState> = {
    magazines: fromMagazines.reducer
};

export const LinksReducers: ActionReducerMap<LinksState> = {
    links: fromLinks.reducer
};

/**
 * A selector function is a map function factory. We pass it parameters and it
 * returns a function that maps from the larger state tree into a smaller
 * piece of state. This selector simply selects the `books` state.
 *
 * Selectors are used with the `select` operator.
 *
 * ```ts
 * class MyComponent {
 *   constructor(state$: Observable<State>) {
 *     this.dashboardState$ = state$.pipe(select(getDashboardState));
 *   }
 * }
 * ```
 */

/**
 * The createFeatureSelector function selects a piece of state from the root of the state object.
 * This is used for selecting feature states that are loaded eagerly or lazily.
 */
export const getMagazinesState = createFeatureSelector<State, MagazinesState>('magazines');


export const getLinksState = createFeatureSelector<State, LinksState>('links');

/**
 * Every reducer module exports selector functions, however child reducers
 * have no knowledge of the overall state tree. To make them usable, we
 * need to make new selectors that wrap them.
 *
 * The createSelector function creates very efficient selectors that are memoized and
 * only recompute when arguments change. The created selectors can also be composed
 * together to select different pieces of state.
 */
export const getAppMagazinesState = createSelector(getMagazinesState, state => state.magazines);



export const getAppLinksState = createSelector(getLinksState, state => state.links);


export const getMagazineItems = createSelector(getAppMagazinesState, fromMagazines.getMagazines);
export const getMagazineSelected = createSelector(getAppMagazinesState, fromMagazines.getSelectedMagazine);
export const getLinkItems = createSelector(getAppLinksState, fromLinks.getLinks);
