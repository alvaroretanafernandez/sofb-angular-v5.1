import {Magazine} from '../../shared/models/magazine.model';
import {LinksActionTypes, LinksActionTypesUnion} from '../actions/links.actions';

/**
 * Interface state
 */
export interface State {
    loaded: boolean;
    loading: boolean;
    links: any;
}

/**
 * getInitialState returns the default initial state
 * for the generated entity state. Initial state
 * additional properties can also be defined.
 */
const initialState: State = {
    loading: false,
    loaded: false,
    links: []
};

/**
 * Dashboard reducers
 * @param {State} state
 * @param {DashboardOrdersActionsUnion} action
 * @returns {State}
 */
export function reducer(state = initialState, action: LinksActionTypesUnion): State {
    switch (action.type) {
        case LinksActionTypes.LinksLoad: {
            return {
                ...state,
                loading: true,
            };
        }
        case LinksActionTypes.LinksLoadSuccess: {
            return {
                ...state,
                loading: false,
                loaded: true,
                links: action.payload
            };
        }
        default: {
            return state;
        }
    }
}

/**
 * helper function
 * @param {State} state
 * @returns {boolean}
 */
export const getLoaded = (state: State) => state.loaded;

/**
 * helper function
 * @param {State} state
 * @returns {boolean}
 */
export const getLoading = (state: State) => state.loading;

/**
 * helper function
 * @param {State} state
 * @returns {Link[]}
 */
export const getLinks = (state: State) => state.links;
