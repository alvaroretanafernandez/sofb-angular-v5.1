import {Magazine} from '../../shared/models/magazine.model';
import {MagazinesActionTypes, MagazinesActionTypesUnion} from '../actions/magazines.actions';

/**
 * Interface state
 */
export interface State {
    loaded: boolean;
    loading: boolean;
    selectedMagazine: any,
    magazines: any;
}

/**
 * getInitialState returns the default initial state
 * for the generated entity state. Initial state
 * additional properties can also be defined.
 */
const initialState: State = {
    loading: false,
    loaded: false,
    selectedMagazine: null,
    magazines: []
};

/**
 * Dashboard reducers
 * @param {State} state
 * @param {DashboardOrdersActionsUnion} action
 * @returns {State}
 */
export function reducer(state = initialState, action: MagazinesActionTypesUnion): State {
    switch (action.type) {
        case MagazinesActionTypes.MagazinesLoad: {
            return {
                ...state,
                loading: true,
            };
        }
        case MagazinesActionTypes.MagazinesLoadSuccess: {
            return {
                ...state,
                loading: false,
                loaded: true,
                magazines: action.payload
            };
        }
        case MagazinesActionTypes.MagazineSelected: {
            return {
                ...state,
                selectedMagazine: state.magazines.filter(mag => mag.id === action.payload),
            };
        }
        default: {
            return state;
        }
    }
}

/**
 * helper function
 * @param {State} state
 * @returns {boolean}
 */
export const getLoaded = (state: State) => state.loaded;

/**
 * helper function
 * @param {State} state
 * @returns {boolean}
 */
export const getLoading = (state: State) => state.loading;

/**
 * helper function
 * @param {State} state
 * @returns {Magazine[]}
 */
export const getMagazines = (state: State) => state.magazines;

/**
 * helper function
 * @param {State} state
 * @returns {Magazine[]}
 */
export const getSelectedMagazine = (state: State) => state.selectedMagazine;
