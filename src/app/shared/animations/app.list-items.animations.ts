import { animate, query, stagger, style, transition, trigger } from '@angular/animations';

export const photosAnimationVertical = (className: string) =>
  trigger('photosAnimationVertical', [
    transition('* => *', [
      query(className, style({ transform: 'translateY(300%)', opacity: 0.6, width: '0%'})),
      query(className,
        stagger('22ms', [
          animate('1200ms', style({ transform: 'translateY(0)', opacity: 1, width: '100%'}))
        ]))
    ])
  ]);

export const photosAnimationHorizontal = (className: string) =>
  trigger('photosAnimationHorizontal', [
    transition('* => *', [
      query(className, style({ transform: 'translateX(-300%)', opacity: 0.6, width: '0%'})),
      query(className,
        stagger('1ms', [
          animate('2200ms', style({ transform: 'translateX(0)', opacity: 1, width: '100%'}))
        ]))
    ])
  ]);

export const photosAnimationFadeIn = (className) =>
  trigger('photosAnimationFadeIn', [
    transition('* => *', [
      query(className, style({ opacity: 0.6, width: '0%'}, )),
      query(className,
        stagger('22ms', [
          animate('1200ms', style({ opacity: 1, width: '100%'}))
        ]))
    ])
  ]);

