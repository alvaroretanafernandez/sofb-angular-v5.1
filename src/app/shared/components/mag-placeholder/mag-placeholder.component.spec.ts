import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MagPlaceholderComponent } from './mag-placeholder.component';

describe('MagPlaceholderComponent', () => {
  let component: MagPlaceholderComponent;
  let fixture: ComponentFixture<MagPlaceholderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MagPlaceholderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MagPlaceholderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
