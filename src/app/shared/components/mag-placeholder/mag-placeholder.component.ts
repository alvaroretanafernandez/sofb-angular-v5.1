import { AfterViewInit, ChangeDetectorRef, Component, Input, OnInit } from '@angular/core';
import { sofbListItemPlaceholderAnimation } from '../../../app.routes.transition';

@Component({
  selector: 'sofb-mag-placeholder',
  templateUrl: './mag-placeholder.component.html',
  styleUrls: ['./mag-placeholder.component.scss'],
  animations: [sofbListItemPlaceholderAnimation]
})
export class MagPlaceholderComponent implements OnInit, AfterViewInit {
@Input() loaded = false;
  animationState = 'none';
  constructor(private cdr: ChangeDetectorRef) { }

  ngOnInit() {
  }

  public generateFake(count: number): Array<number> {
    const indexes = [];
    for (let i = 0; i < count; i++) {
      indexes.push(i);
    }
    return indexes;
  }
  ngAfterViewInit() {
    this.animationState = 'normal';
    setTimeout(() => { this.animationState = 'invisible'; }, 100);
    this.cdr.detectChanges();
  }
}
