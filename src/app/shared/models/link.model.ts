export interface Link {
    id: number;
    _id: string;
    desc: string;
    title: string;
    img: string;
    url: string;
}
