export interface Magazine {
    id: number;
    _id: string;
    url: string;
    frame: string;
    img: string;
    published: string;
}
