import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
export class Link {
    constructor(public id: number,
                public _id: string,
                public desc: string,
                public title: string,
                public img: string,
                public url: string) {
    }
}

@Injectable()
export class LinksService {

    public links: Link[];

    constructor(public http: HttpClient) {
    }


    getLinks(): Observable<any> {
        return this.http.get('https://api.stokedonfixedbikes.com/api/links');
    }




}
