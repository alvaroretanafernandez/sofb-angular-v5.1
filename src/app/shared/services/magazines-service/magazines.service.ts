import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';

export class Magazine {
    constructor(public id: number,
                public _id: string,
                public url: string,
                public frame: string,
                public img: string,
                public published: string,) {
    }
}

@Injectable()
export class MagazinesService {

    public lastMag = '22';
    public magazines: Magazine[];

    constructor(public http: HttpClient) {
    }


    getMagazines(): Observable<any> {
        return this.http.get('https://api.stokedonfixedbikes.com/api/magazines');
    }


    getMagazine(id: number | string): Observable<any> {
        return this.http.get('https://api.stokedonfixedbikes.com/api/magazines/' + id);
    }

}
