import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';

export class Video {
    constructor(public videoId: number,
                public _id: string,
                public type: string,
                public title: string,
                public preview: string,
                public description: string) {
    }
}

@Injectable()
export class VideosService {

    public videos: Video[];

    constructor(public http: HttpClient) {
    }


    getVideos(): Observable<any> {
        return this.http.get('https://api.stokedonfixedbikes.com/api/videos');
    }


}
