import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {SafePipe} from './pipes/safe.pipe';
import {AwesomePipe} from './pipes/awesome.pipe';
import {HighlightDirective} from './pipes/highlight.directive';
import {VideosService} from './services/videos-service/videos.service';
import {LinksService} from './services/links-service/links.service';
import {MagazinesService} from './services/magazines-service/magazines.service';
import {CollapseModule} from 'ngx-bootstrap/collapse/collapse.module';
import {CarouselModule} from 'ngx-bootstrap/carousel/carousel.module';
import {FormsModule} from '@angular/forms';
import {CoreModule} from '../core/core.module';
import { SocialComponent } from './components/social/social.component';
import { SubheaderComponent } from './components/subheader/subheader.component';
import { MagPlaceholderComponent } from './components/mag-placeholder/mag-placeholder.component';
@NgModule({
    imports: [
        CommonModule
    ],
    declarations: [
        AwesomePipe,
        HighlightDirective,
        SafePipe,
        SocialComponent,
        SubheaderComponent,
        MagPlaceholderComponent],
    exports: [
        CommonModule,
        AwesomePipe,
        HighlightDirective,
        SafePipe,
        SocialComponent,
        SubheaderComponent,
        MagPlaceholderComponent
    ],
    providers: [
        MagazinesService,
        LinksService,
        VideosService
    ]
})
export class SharedModule {

    static forRoot() {
        return {
            ngModule: SharedModule,
            providers: [
                MagazinesService,
                LinksService,
                VideosService
            ]
        };
    }
}
